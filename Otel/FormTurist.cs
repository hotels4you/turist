﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Hotel.architecture;
using Hotel.architecture.storagedriver;
using Hotel.architecture.entities;

namespace Hotel
{
    public partial class FormTurist : Form
    {
        public TouristEntity refTurist;
        public FormTurist()
        {
            InitializeComponent();
        }

        public void SetData(TouristEntity pTurist)
        {
            txtTuristName.Text = pTurist.name;
            txtTuristLastname.Text = pTurist.lastname; 
            txtTuristPassport.Text = pTurist.passport;
            birthDatePicker.Value = pTurist.birthdate;
            DateInPicker.Value = pTurist.dateIn;
            DateOutPicker.Value = pTurist.dateOut;
        }
        public TouristEntity EditData(TouristEntity tourist)
        {
            tourist.name = txtTuristName.Text;
            tourist.lastname = txtTuristLastname.Text;
            tourist.passport = txtTuristPassport.Text;
            tourist.birthdate = birthDatePicker.Value;
            tourist.dateIn = DateInPicker.Value;
            tourist.dateOut = DateOutPicker.Value;
            return tourist;
        }
        public TouristEntity GetData()
        {
            TouristEntity tourist = new TouristEntity();
            tourist.name = txtTuristName.Text;
            tourist.lastname = txtTuristLastname.Text;
            tourist.passport = txtTuristPassport.Text;
            tourist.birthdate = birthDatePicker.Value;
            tourist.dateIn = DateInPicker.Value;
            tourist.dateOut = DateOutPicker.Value;
            return tourist;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtTuristName.Text = string.Empty;
            txtTuristLastname.Text = string.Empty;
            txtTuristPassport.Text = string.Empty;
        }

    }
}
