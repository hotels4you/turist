﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Hotel.architecture;
using Hotel.architecture.storagedriver;
using Hotel.architecture.entities;

namespace Hotel
{
    public partial class FormHotel : Form
    {
        public FormHotel()
        {
            InitializeComponent();
        }

        public HotelEntity GetData()
        {
            HotelEntity hotel = new HotelEntity();
            hotel.name = txtHotelName.Text;
            return hotel;
        }
        public HotelEntity EditData(HotelEntity hotel) {
            hotel.name = txtHotelName.Text;
            return hotel;
        }

        public void SetData(HotelEntity hotel)
        {
            txtHotelName.Text = hotel.name;
          
        }
    }
}
