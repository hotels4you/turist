﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Hotel.architecture;
using Hotel.architecture.storagedriver;
using Hotel.architecture.entities;

namespace Hotel
{
    public partial class FormNomer : Form
    {
        public RoomEntity refNomer;
        public FormNomer()
        {
            InitializeComponent();
          
        }
        public void SetData(RoomEntity pNomer)
        {
            sNomer.Text = pNomer.id.ToString();
            sCapacity.Text = pNomer.capacity;
            sPrice.Text = pNomer.price.ToString();
        }

        public RoomEntity GetData()
        {
            RoomEntity room = new RoomEntity();
           room.number = sNomer.Text;         
           room.capacity = sCapacity.Text;
           room.price = sPrice.Text;
           return room;
        }
        public RoomEntity EditData(RoomEntity room)
        {
            room.number = sNomer.Text;
            room.capacity = sCapacity.Text;
            room.price = sPrice.Text;
            return room;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void capacity_TextChanged(object sender, EventArgs e)
        {

        }

        private void price_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
        
        }
    }
}
