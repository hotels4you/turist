﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Hotel.architecture;
using Hotel.architecture.storagedriver;
using Hotel.architecture.entities;

namespace Hotel
{
    public partial class FormService : Form
    {
        public FormService()
        {
            InitializeComponent();
        }
        public ServiceEntity GetData()
        {
            ServiceEntity service = new ServiceEntity();
            service.name = textBoxName.Text;
            service.price = textBoxPrice.Text;
            return service;
        }
        public ServiceEntity EditData(ServiceEntity service)
        {
            service.name = textBoxName.Text;
            service.price = textBoxPrice.Text;
            return service;
        }

        public void SetData(ServiceEntity service)
        {
            textBoxName.Text = service.name;
            textBoxPrice.Text = service.price;

        }
    }
}
