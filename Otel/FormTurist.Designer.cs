﻿namespace Hotel
{
    partial class FormTurist
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTurist = new System.Windows.Forms.Button();
            this.txtTuristName = new System.Windows.Forms.TextBox();
            this.txtTuristLastname = new System.Windows.Forms.TextBox();
            this.txtTuristPassport = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.birthDatePicker = new System.Windows.Forms.DateTimePicker();
            this.DateInPicker = new System.Windows.Forms.DateTimePicker();
            this.DateOutPicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnTurist
            // 
            this.btnTurist.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnTurist.Location = new System.Drawing.Point(7, 220);
            this.btnTurist.Name = "btnTurist";
            this.btnTurist.Size = new System.Drawing.Size(114, 29);
            this.btnTurist.TabIndex = 0;
            this.btnTurist.Text = "ОК";
            this.btnTurist.UseVisualStyleBackColor = true;
            // 
            // txtTuristName
            // 
            this.txtTuristName.Location = new System.Drawing.Point(99, 21);
            this.txtTuristName.Name = "txtTuristName";
            this.txtTuristName.Size = new System.Drawing.Size(277, 20);
            this.txtTuristName.TabIndex = 2;
            // 
            // txtTuristLastname
            // 
            this.txtTuristLastname.Location = new System.Drawing.Point(100, 47);
            this.txtTuristLastname.Name = "txtTuristLastname";
            this.txtTuristLastname.Size = new System.Drawing.Size(276, 20);
            this.txtTuristLastname.TabIndex = 3;
            // 
            // txtTuristPassport
            // 
            this.txtTuristPassport.Location = new System.Drawing.Point(99, 73);
            this.txtTuristPassport.Name = "txtTuristPassport";
            this.txtTuristPassport.Size = new System.Drawing.Size(277, 20);
            this.txtTuristPassport.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Имя";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Фамилия";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Номер паспорта";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(127, 220);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(138, 31);
            this.button2.TabIndex = 8;
            this.button2.Text = "Очистить поля";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Год рождения";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(271, 221);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 30);
            this.button1.TabIndex = 16;
            this.button1.Text = "отменить";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // birthDatePicker
            // 
            this.birthDatePicker.Location = new System.Drawing.Point(99, 101);
            this.birthDatePicker.Name = "birthDatePicker";
            this.birthDatePicker.Size = new System.Drawing.Size(277, 20);
            this.birthDatePicker.TabIndex = 17;
            // 
            // DateInPicker
            // 
            this.DateInPicker.Location = new System.Drawing.Point(99, 127);
            this.DateInPicker.Name = "DateInPicker";
            this.DateInPicker.Size = new System.Drawing.Size(277, 20);
            this.DateInPicker.TabIndex = 17;
            // 
            // DateOutPicker
            // 
            this.DateOutPicker.Location = new System.Drawing.Point(100, 153);
            this.DateOutPicker.Name = "DateOutPicker";
            this.DateOutPicker.Size = new System.Drawing.Size(277, 20);
            this.DateOutPicker.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Дата въезда";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 153);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Дата выезда";
            // 
            // FormTurist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 261);
            this.Controls.Add(this.DateOutPicker);
            this.Controls.Add(this.DateInPicker);
            this.Controls.Add(this.birthDatePicker);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTuristPassport);
            this.Controls.Add(this.txtTuristLastname);
            this.Controls.Add(this.txtTuristName);
            this.Controls.Add(this.btnTurist);
            this.Name = "FormTurist";
            this.Text = "Турист";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTurist;
        private System.Windows.Forms.TextBox txtTuristName;
        private System.Windows.Forms.TextBox txtTuristLastname;
        private System.Windows.Forms.TextBox txtTuristPassport;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker birthDatePicker;
        private System.Windows.Forms.DateTimePicker DateInPicker;
        private System.Windows.Forms.DateTimePicker DateOutPicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

