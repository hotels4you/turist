﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hotel.architecture.entities
{
    public class EntityList
    {
        public List<entities.HotelEntity> hotels;
        public List<entities.TouristEntity> tourists;

        public EntityList()
        {
            this.hotels = new List<entities.HotelEntity>();
            this.tourists = new List<entities.TouristEntity>();
        }
    }
}
