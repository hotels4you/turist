﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace Hotel.architecture.entities
{
    public abstract class AbstractEntity
    {
        private static int _id;
        public int id {get; set;}

        public AbstractEntity() {
            id = Interlocked.Increment(ref _id);
        }
    }
}
