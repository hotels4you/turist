﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Hotel.architecture.entities
{
    public class RoomEntity : AbstractEntity
    {
        private static int _id;
        public int id { get; set; }
        public string number;
        public string price;
        public string capacity;

        public RoomEntity()
        {
            id = Interlocked.Increment(ref _id);
        }
    }
}
