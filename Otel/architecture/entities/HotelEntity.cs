﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Hotel.architecture.entities
{


    public class HotelEntity : AbstractEntity
    {
        private static int _id;
        public int id { get; set; }
        public string name;
        public List<RoomEntity> Rooms;
        public List<ServiceEntity> services;

        public HotelEntity()
        {
            Rooms = new List<RoomEntity>();
            services = new List<ServiceEntity>();
            id = Interlocked.Increment(ref _id);
        }
        ~HotelEntity()
        {
            Rooms.Clear();
            services.Clear();
        }
    }
}
