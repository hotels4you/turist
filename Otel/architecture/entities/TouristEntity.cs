﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Hotel.architecture.entities
{

    public class TouristEntity : AbstractEntity
    {
        private static int _id;
        public int id { get; set; }
       public string passport;
       public string name;
       public string lastname;
       public DateTime dateIn;
       public DateTime dateOut;
       public DateTime birthdate;
       public RoomEntity room;

       public TouristEntity()
       {
           id = Interlocked.Increment(ref _id);
       }
   }

}
