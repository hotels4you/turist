﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Hotel.architecture.entities
{
    public class ServiceEntity : AbstractEntity
    {
        private static int _id;
        public int id { get; set; }
        public string name;
        public string price;

        public ServiceEntity()
        {
            id = Interlocked.Increment(ref _id);
        }
    }
}
