﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hotel.architecture.entities;

namespace Hotel.architecture
{
    class HotelController
    {
        private static HotelController _instance;
        private DB db;

        public EntityList entityList {get; private set;}

        private HotelController() {
            entityList = new EntityList();
            db = new DB(new storagedriver.XMLStorageDriver());
        }
        public static HotelController getInstance()
        {
            if (_instance == null)
            {
                return new HotelController();
            }
            return _instance;
        }
        public void addHotel(HotelEntity hotel)
        {
            entityList.hotels.Add(hotel);
        }
        public void removeHotel(HotelEntity hotel)
        {
            entityList.hotels.Remove(hotel);
        }
        public void addRoom(HotelEntity hotel, RoomEntity room)
        {
            hotel.Rooms.Add(room);
        }
        public void removeRoom(HotelEntity hotel, RoomEntity room)
        {
            foreach (TouristEntity t in entityList.tourists)
            {
                if (t.room.Equals(room))
                    t.room = null;
            }
            hotel.Rooms.Remove(room);
        }
        public void addTourist(TouristEntity tourist)
        {
            entityList.tourists.Add(tourist);
        }
        public void removeTourist(TouristEntity tourist)
        {
            entityList.tourists.Remove(tourist);
        }
        public bool checkIn(TouristEntity tourist, RoomEntity room)
        {
            int i = 0;
            foreach (TouristEntity t in entityList.tourists)
            {
                if (t.room == room)
                {
                    i = i + 1;
                }
            }
            if (i >= Convert.ToInt32(room.capacity))
            {
                return false;
            }
            tourist.room = room;
            return true;
        }
        public void checkOut(TouristEntity tourist)
        {
            tourist.room = null;
        }
        public void addService(HotelEntity hotel, ServiceEntity service)
        {
            hotel.services.Add(service);
        }
        public void removeService(HotelEntity hotel, ServiceEntity service)
        {
            hotel.services.Remove(service);
        }
        public void save()
        {
            db.save(entityList);
        }
        public void load()
        {
            entityList = db.load();
        }
    }
}
