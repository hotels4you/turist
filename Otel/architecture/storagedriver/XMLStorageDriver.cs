﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace Hotel.architecture.storagedriver
{
    class XMLStorageDriver : StorageDriver
    {
        public override entities.EntityList getData()
        {
            entities.EntityList entityList = null;

            XmlSerializer serializer = new XmlSerializer(typeof(entities.EntityList));

            StreamReader reader = new StreamReader("Xmltest.xml");
            entityList = (entities.EntityList)serializer.Deserialize(reader);
            reader.Close();
            return entityList;
        }
        public override bool setData(entities.EntityList data)
        {
            XmlSerializer serializer = new XmlSerializer(data.GetType());
            using (TextWriter writer = new StreamWriter("Xmltest.xml"))
            {
                serializer.Serialize(writer, data);
            } 
            return true;
        }
        public override bool connect()
        {
            return true;
        }
        public override bool disconnect()
        {
            return true;
        }
    }
}
