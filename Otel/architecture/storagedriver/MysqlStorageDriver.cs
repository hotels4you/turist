﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hotel.architecture.storagedriver
{
    class MysqlStorageDriver : StorageDriver
    {
        public override entities.EntityList getData()
        {
            return new entities.EntityList();
        }
        public override bool setData(entities.EntityList data)
        {
            return true;
        }
        public override bool connect()
        {
            return true;
        }
        public override bool disconnect()
        {
            return true;
        }
    }
}
