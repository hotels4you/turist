﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hotel.architecture.storagedriver
{
    abstract class StorageDriver
    {
        public abstract entities.EntityList getData();
        public abstract bool setData(entities.EntityList data);
        public abstract bool connect();
        public abstract bool disconnect();
    }
}
