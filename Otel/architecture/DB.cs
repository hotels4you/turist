﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hotel.architecture.storagedriver;
using Hotel.architecture.entities;

namespace Hotel.architecture
{
    class DB
    {
        private StorageDriver storageDriver;

        public DB(StorageDriver storageDriver)
        {
            this.storageDriver = storageDriver;
        }
        public bool save(EntityList data)
        {
            return this.storageDriver.setData(data);
        }
        public EntityList load()
        {
            return this.storageDriver.getData();
        }
    }
}
