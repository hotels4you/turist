﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Hotel.architecture;
using Hotel.architecture.storagedriver;
using Hotel.architecture.entities;

namespace Hotel
{
    public partial class FormMain : Form
    {
        private HotelController controller;
        public FormMain()
        {
            InitializeComponent();
            controller = HotelController.getInstance();
            EnableButtons();
        }

        private void RefreshHotels(HotelEntity pSelHotel = null)
        {
            listHotel.Items.Clear();
            listNomer.Items.Clear();
            listTuristInNomer.Items.Clear();

            foreach (HotelEntity h in controller.entityList.hotels)
            {
                ListViewItem item1 = new ListViewItem(h.id.ToString());
                item1.SubItems.Add(h.name);
                item1.Tag = h;
                listHotel.Items.Add(item1);
                if (pSelHotel != null)
                   if (pSelHotel==h)
                   {
                       item1.Selected = true;
                   }
            }
            if ((pSelHotel == null)&&(listHotel.Items.Count>0))
                  listHotel.Items[0].Selected = true;

            EnableButtons();
        }

        private void RefreshRooms(HotelEntity pCurHotel = null, RoomEntity pSelNomer = null)
        {
            listNomer.Items.Clear();
            listTuristInNomer.Items.Clear();
            if (pCurHotel == null)
            {
                pCurHotel = controller.entityList.hotels[0];
            }
            foreach (RoomEntity n in pCurHotel.Rooms)
            {
                ListViewItem item1 = new ListViewItem(n.id.ToString());
                item1.SubItems.Add(n.capacity);
                item1.SubItems.Add(n.price);
                item1.Tag = n;
                listNomer.Items.Add(item1);
                if (pSelNomer != null)
                  if (pSelNomer==n)
                  {
                      item1.Selected = true;
                  }
            }
            if ((pSelNomer == null) & (listNomer.Items.Count > 0))
                listNomer.Items[0].Selected = true;
            
            EnableButtons();
        }

        private void RefreshTuristInNomer(RoomEntity room = null)
        {
            listTuristInNomer.Items.Clear();
            if (room == null)
            {
                room = controller.entityList.hotels[0].Rooms[0];
            }
            foreach (TouristEntity t in controller.entityList.tourists)
            {
                if (t.room == null)
                {
                    continue;
                }
                if (t.room.id == room.id)
                {
                   ListViewItem item1 = new ListViewItem(t.name);
                   item1.SubItems.Add(t.lastname);
                   item1.SubItems.Add(t.passport.ToString());
                   item1.SubItems.Add(t.birthdate.ToString("yyyy-MM-dd"));
                   item1.SubItems.Add(t.dateIn.ToString("yyyy-MM-dd"));
                   item1.SubItems.Add(t.dateOut.ToString("yyyy-MM-dd"));
                   item1.Tag = t;
                   listTuristInNomer.Items.Add(item1);
                }
            }
            EnableButtons();
        }

        private void RefreshServices(HotelEntity selHotel = null) {
            listServices.Items.Clear();
            if (selHotel == null)
            {
                selHotel = controller.entityList.hotels[0];
            }
            foreach (ServiceEntity n in selHotel.services)
            {
                ListViewItem item1 = new ListViewItem(n.name);
                item1.SubItems.Add(n.price);
                item1.Tag = n;
                listServices.Items.Add(item1);
            }

            EnableButtons();

        }

        private void RefreshTurists(TouristEntity pSelTurist = null)
        {
            listTurist.Items.Clear();
            foreach (TouristEntity t in controller.entityList.tourists)
            {
                ListViewItem item1 = new ListViewItem(t.name);
                item1.SubItems.Add(t.lastname);
                item1.SubItems.Add(t.passport.ToString());
                item1.SubItems.Add(t.birthdate.ToString("yyyy-MM-dd"));
                item1.SubItems.Add(t.dateIn.ToString("yyyy-MM-dd"));
                item1.SubItems.Add(t.dateOut.ToString("yyyy-MM-dd"));
                item1.Tag = t;
                listTurist.Items.Add(item1);
            }
            if (pSelTurist != null)
            {
                // выбрать указанный номер в списке
            }
            EnableButtons();
        }

        public void EnableButtons()
        {
            bool hSel = (listHotel.SelectedItems.Count != 0);
            btnEditHotel.Enabled = hSel;
            btnDelHotel.Enabled = hSel;
            btnNomer.Enabled = hSel;
            addServiceButton.Enabled = hSel;

            bool nSel = (listNomer.SelectedItems.Count != 0);
            btnChangeNomer.Enabled = nSel;
            btnDelNomer.Enabled = nSel;

            bool tSel = (listTurist.SelectedItems.Count != 0);
            btnEditTurist.Enabled = tSel;
            btnDelTurist.Enabled = tSel;

            btnCheckIn.Enabled = nSel & tSel;

            bool thSel = (listTuristInNomer.SelectedItems.Count != 0);
            btnCheckOut.Enabled = thSel;

            bool sSel = (listServices.SelectedItems.Count != 0); ;
            removeServiceButton.Enabled = sSel;
        }


        public void btnHotel_Click(object sender, EventArgs e)
        {
          FormHotel f = new FormHotel();
          if (f.ShowDialog(this) == DialogResult.OK)
          {
              HotelEntity h = f.GetData();
              controller.addHotel(h);
              RefreshHotels(h);        
          }
          f.Dispose();
        }

        private void btnEditHotel_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = (ListViewItem)listHotel.SelectedItems[0];
            if (item1 == null)
                return;
            FormHotel f = new FormHotel();
            HotelEntity h = (HotelEntity)item1.Tag;
            f.SetData(h);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.EditData(h);
                RefreshHotels(h);
            }          

        }

        private void btnDelHotel_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = (ListViewItem)listHotel.SelectedItems[0];
            controller.removeHotel((HotelEntity)item1.Tag);
            listHotel.Items.Clear();
            listTuristInNomer.Items.Clear();
            RefreshHotels(null);
        }


        private void btnNomer_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem curHotel = (ListViewItem)listHotel.SelectedItems[0];
            if (curHotel == null)
                return;

            //RoomEntity room = new RoomEntity(); //
            FormNomer f = new FormNomer();
            //f.SetData(pNomer); //
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                RoomEntity room = f.GetData(); //
                HotelEntity h = (HotelEntity)curHotel.Tag; //
                controller.addRoom(h, room); // ДОБАВЛЕНИЕ НОМЕРА К ОТЕЛЮ

                RefreshRooms(h, room);
             }
             //f.Dispose(); ???
        }

        private void btnChangeNomer_Click(object sender, EventArgs e)
        {
            if (listNomer.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = listNomer.SelectedItems[0];
            if (item1 == null)
                return;
            ListViewItem htl = (ListViewItem)listHotel.SelectedItems[0];
            if (htl == null)
                return;
            FormNomer f = new FormNomer();
            RoomEntity n = (RoomEntity)item1.Tag;
            f.SetData(n);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.EditData(n);
                RefreshRooms((HotelEntity)htl.Tag, n);
            }          

        }
        
        private void btnDelNomer_Click(object sender, EventArgs e)
        {
            if ((listHotel.SelectedItems.Count == 0) || (listNomer.SelectedItems.Count == 0))
                return;
            ListViewItem itemNomer = listNomer.SelectedItems[0];
            if (itemNomer == null)
                return;
            ListViewItem itemHotel = listHotel.SelectedItems[0];
            if (itemHotel == null)
                return;

            RoomEntity n = (RoomEntity)itemNomer.Tag;
            HotelEntity h = (HotelEntity)itemHotel.Tag;

            controller.removeRoom(h, n);

            RefreshRooms(h, null);

        }

        private void btnAddTurist_Click(object sender, EventArgs e)
        {
            FormTurist f = new FormTurist();

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                controller.addTourist(f.GetData());
                RefreshTurists(null);
            }          
        }
        private void btnEditTurist_Click(object sender, EventArgs e)
        {
            if (listTurist.SelectedItems == null)
                return;
            ListViewItem item1 = listTurist.SelectedItems[0];
            if (item1 == null)
                return;
            FormTurist f = new FormTurist();
            TouristEntity t = (TouristEntity)item1.Tag;
            f.SetData( t );

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.EditData(t);
                RefreshTurists();
            }          
        }
        private void btnDelTurist_Click(object sender, EventArgs e)
        {
            ListViewItem item1 = listTurist.SelectedItems[0];
            if (item1 == null)
                return;
            TouristEntity t = (TouristEntity)item1.Tag;
            controller.removeTourist(t);
            RefreshTurists();
        }
        private void listHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            listNomer.Items.Clear();
            listTuristInNomer.Items.Clear();
            EnableButtons();
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem curHotel = (ListViewItem)listHotel.SelectedItems[0];
            if (curHotel != null)
            {
                RefreshRooms((HotelEntity)curHotel.Tag, null);
                RefreshServices((HotelEntity)curHotel.Tag);
            }
        }

         private void listNomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            listTuristInNomer.Items.Clear();
            EnableButtons();
            if (listNomer.SelectedItems.Count == 0)
                return;
            ListViewItem item = (ListViewItem)listNomer.SelectedItems[0];
            RoomEntity n = (RoomEntity)item.Tag;
            RefreshTuristInNomer(n);
        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            if (listNomer.SelectedItems.Count == 0)
                return;
            if (listTurist.SelectedItems.Count == 0)
                return;

            ListViewItem itemTurist = listTurist.SelectedItems[0];
            ListViewItem itemNomer = listNomer.SelectedItems[0];
            TouristEntity t = (TouristEntity)itemTurist.Tag;
            RoomEntity n = (RoomEntity)itemNomer.Tag;

            if (!controller.checkIn(t, n))
            {
                MessageBox.Show("Перегруженно");
            }

            RefreshTuristInNomer(n);
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            if (listTuristInNomer.SelectedItems.Count == 0)
                return;
            if (listNomer.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = listTuristInNomer.SelectedItems[0];
            TouristEntity t = (TouristEntity)item1.Tag;
            RoomEntity n = t.room;
            controller.checkOut(t);
            RefreshTuristInNomer(n);
        }

        private void listTurist_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }

        private void listTuristInNomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }

        private void save_Click(object sender, EventArgs e)
        {
            controller.save();
        }

        private void load_click(object sender, EventArgs e)
        {
            controller.load();
            RefreshRooms();
            RefreshHotels();
            RefreshTurists();
            RefreshServices(null);
            RefreshTuristInNomer();
        }

        private void addServiceButton_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;

            ListViewItem itemHotel = listHotel.SelectedItems[0];
            HotelEntity h = (HotelEntity)itemHotel.Tag;
            FormService f = new FormService();

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                controller.addService(h, f.GetData());
                RefreshServices(h);
            }  
        }

        private void listServices_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }

        private void editServiceButton_Click(object sender, EventArgs e)
        {
            if (listServices.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = listServices.SelectedItems[0];
            if (item1 == null)
                return;
            ListViewItem htl = (ListViewItem)listHotel.SelectedItems[0];
            if (htl == null)
                return;
            FormService f = new FormService();
            ServiceEntity n = (ServiceEntity)item1.Tag;
            f.SetData(n);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.EditData(n);
                RefreshServices((HotelEntity)htl.Tag);
            }   
        }

        private void removeServiceButton_Click(object sender, EventArgs e)
        {
            if ((listHotel.SelectedItems.Count == 0) || (listServices.SelectedItems.Count == 0))
                return;
            ListViewItem itemService = listServices.SelectedItems[0];
            if (itemService == null)
                return;
            ListViewItem itemHotel = listHotel.SelectedItems[0];
            if (itemHotel == null)
                return;

            ServiceEntity n = (ServiceEntity)itemService.Tag;
            HotelEntity h = (HotelEntity)itemHotel.Tag;

            controller.removeService(h, n);

            RefreshServices(h);
        }

    }
}
