﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Otel
{
    public partial class FormNomer : Form
    {
        public Nomer refNomer;
        public FormNomer()
        {
            InitializeComponent();
          
        }
        public void SetData(Nomer pNomer)
        {
            sNomer.Text = pNomer.pnomer.ToString();
            sCapacity.Text = pNomer.capacity.ToString();
            sPrice.Text = pNomer.price.ToString();
        }
        
        public void GetData(Nomer pNomer)
        {
           pNomer.pnomer = int.Parse(sNomer.Text);         
           pNomer.capacity = int.Parse(sCapacity.Text);
           pNomer.price = int.Parse(sPrice.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void capacity_TextChanged(object sender, EventArgs e)
        {

        }

        private void price_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
        
        }
    }
}
