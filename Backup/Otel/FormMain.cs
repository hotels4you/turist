﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Otel
{
    public partial class FormMain : Form
    {
        public List<Hotel> Hotels;
        public List<Turist> Turists;
        public FormMain()
        {
            InitializeComponent();
            Hotels = new List<Hotel>();
            Turists = new List<Turist>();
            EnableButtons();
        }
        
        private void RefreshHotels(Hotel pSelHotel)
        {
            listHotel.Items.Clear();
            listNomer.Items.Clear();
            listTuristInNomer.Items.Clear();

            foreach (Hotel h in Hotels)
            {
                ListViewItem item1 = new ListViewItem(h.name);
                item1.Tag = h;
                listHotel.Items.Add(item1);
                if (pSelHotel != null)
                   if (pSelHotel==h)
                   {
                       item1.Selected = true;
                   }
            }
            if ((pSelHotel == null)&&(listHotel.Items.Count>0))
                  listHotel.Items[0].Selected = true;

            EnableButtons();
        }

        private void RefreshNomera(Hotel pCurHotel, Nomer pSelNomer)
        {
            listNomer.Items.Clear();
            listTuristInNomer.Items.Clear();
            foreach (Nomer n in pCurHotel.Nomera)
            {
                ListViewItem item1 = new ListViewItem(n.pnomer.ToString());
                item1.SubItems.Add(n.capacity.ToString());
                item1.SubItems.Add(n.price.ToString());
                item1.Tag = n;
                listNomer.Items.Add(item1);
                if (pSelNomer != null)
                  if (pSelNomer==n)
                  {
                      item1.Selected = true;
                  }
            }
            if ((pSelNomer == null) & (listNomer.Items.Count > 0))
                listNomer.Items[0].Selected = true;
            
            EnableButtons();
        }

        private void RefreshTuristInNomer(Nomer pCurNomer)
        {
            listTuristInNomer.Items.Clear();
            foreach (Turist t in Turists)
            {
                if (t.NomerOfTurist!=null)
                    if (t.NomerOfTurist.Equals(pCurNomer))
                    {
                       ListViewItem item1 = new ListViewItem(t.name);
                       item1.SubItems.Add(t.lastname);
                       item1.SubItems.Add(t.passport.ToString());
                       item1.SubItems.Add(t.day.ToString() + "." + t.month.ToString() + "." + t.year.ToString());
                       item1.Tag = t;
                       listTuristInNomer.Items.Add(item1);
                    }
            }
            EnableButtons();
        }

        private void RefreshTurists(Turist pSelTurist)
        {
            listTurist.Items.Clear();
            foreach (Turist t in Turists)
            {
                ListViewItem item1 = new ListViewItem(t.name);
                item1.SubItems.Add(t.lastname);
                item1.SubItems.Add(t.passport.ToString());
                item1.SubItems.Add(t.day.ToString() + "." + t.month.ToString() + "." + t.year.ToString());
                item1.Tag = t;
                listTurist.Items.Add(item1);
            }
            if (pSelTurist != null)
            {
                // выбрать указанный номер в списке
            }
            EnableButtons();
        }

        public void EnableButtons()
        {
            bool hSel = (listHotel.SelectedItems.Count != 0);
            btnEditHotel.Enabled = hSel;
            btnDelHotel.Enabled = hSel;
            btnNomer.Enabled = hSel;

            bool nSel = (listNomer.SelectedItems.Count != 0);
            btnChangeNomer.Enabled = nSel;
            btnDelNomer.Enabled = nSel;

            bool tSel = (listTurist.SelectedItems.Count != 0);
            btnEditTurist.Enabled = tSel;
            btnDelTurist.Enabled = tSel;

            btnCheckIn.Enabled = nSel & tSel;

            bool thSel = (listTuristInNomer.SelectedItems.Count != 0);
            btnCheckOut.Enabled = thSel;

        }


        public void btnHotel_Click(object sender, EventArgs e)
        {
          FormHotel f = new FormHotel();
          Hotel h = new Hotel();
          if (f.ShowDialog(this) == DialogResult.OK)
          {
              f.GetData(h);
              Hotels.Add(h);
              RefreshHotels(h);              
          }
          f.Dispose();
        }

        private void btnEditHotel_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = (ListViewItem)listHotel.SelectedItems[0];
            if (item1 == null)
                return;
            FormHotel f = new FormHotel();
            Hotel h = (Hotel)item1.Tag;
            f.SetData(h);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.GetData(h);
                RefreshHotels(h);
            }          

        }

        private void btnDelHotel_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = (ListViewItem)listHotel.SelectedItems[0];
            Hotels.Remove((Hotel)item1.Tag);
            listHotel.Items.Clear();
            listTuristInNomer.Items.Clear();
            RefreshHotels(null);
        }


        private void btnNomer_Click(object sender, EventArgs e)
        {
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem curHotel = (ListViewItem)listHotel.SelectedItems[0];
            if (curHotel == null)
                return;

            Nomer pNomer = new Nomer(); //
            FormNomer f = new FormNomer();
            //f.SetData(pNomer); //
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.GetData(pNomer); //
                Hotel h = (Hotel)curHotel.Tag; //
                h.Nomera.Add(pNomer); // ДОБАВЛЕНИЕ НОМЕРА К ОТЕЛЮ

                RefreshNomera(h,pNomer);
             }
             //f.Dispose(); ???
        }

        private void btnChangeNomer_Click(object sender, EventArgs e)
        {
            if (listNomer.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = listNomer.SelectedItems[0];
            if (item1 == null)
                return;
            FormNomer f = new FormNomer();
            Nomer n = (Nomer)item1.Tag;
            f.SetData(n);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.GetData(n);
                RefreshHotels(null);
            }          

        }
        
        private void btnDelNomer_Click(object sender, EventArgs e)
        {
            if ((listHotel.SelectedItems.Count == 0) || (listNomer.SelectedItems.Count == 0))
                return;
            ListViewItem itemNomer = listNomer.SelectedItems[0];
            if (itemNomer == null)
                return;
            ListViewItem itemHotel = listHotel.SelectedItems[0];
            if (itemHotel == null)
                return;

            Nomer n = (Nomer)itemNomer.Tag;
            Hotel h = (Hotel)itemHotel.Tag;
            foreach (Turist t in Turists)
            {
                if (t.NomerOfTurist.Equals(n))
                   t.NomerOfTurist = null;
            }
            h.Nomera.Remove(n);

            RefreshNomera(h, null);

        }

        private void btnAddTurist_Click(object sender, EventArgs e)
        {
            Turist pTurist = new Turist();
            FormTurist f = new FormTurist();
            f.SetData(pTurist);

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.GetData(pTurist);
                Turists.Add(pTurist);
                RefreshTurists(null);
            }          
        }
        private void btnEditTurist_Click(object sender, EventArgs e)
        {
            if (listTurist.SelectedItems == null)
                return;
            ListViewItem item1 = listTurist.SelectedItems[0];
            if (item1 == null)
                return;
            FormTurist f = new FormTurist();
            Turist t = (Turist)item1.Tag;
            f.SetData( t );

            if (f.ShowDialog(this) == DialogResult.OK)
            {
                f.GetData(t);
                RefreshTurists(null);
            }          
        }
        private void btnDelTurist_Click(object sender, EventArgs e)
        {
            ListViewItem item1 = listTurist.SelectedItems[0];
            if (item1 == null)
                return;
            Turist t = (Turist)item1.Tag;
            Turists.Remove(t);
            RefreshTurists(null);
        }




        private void listHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            listNomer.Items.Clear();
            listTuristInNomer.Items.Clear();
            EnableButtons();
            if (listHotel.SelectedItems.Count == 0)
                return;
            ListViewItem curHotel = (ListViewItem)listHotel.SelectedItems[0];
            if (curHotel != null)
            {
                RefreshNomera((Hotel)curHotel.Tag, null);
            }
        }

         private void listNomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            listTuristInNomer.Items.Clear();
            EnableButtons();
            if (listNomer.SelectedItems.Count == 0)
                return;
            ListViewItem item = (ListViewItem)listNomer.SelectedItems[0];
            Nomer n = (Nomer)item.Tag;
            RefreshTuristInNomer(n);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnCheckIn_Click(object sender, EventArgs e)
        {
            if (listNomer.SelectedItems.Count == 0)
                return;
            if (listTurist.SelectedItems.Count == 0)
                return;

            ListViewItem itemTurist = listTurist.SelectedItems[0];
            ListViewItem itemNomer = listNomer.SelectedItems[0];
            Turist t = (Turist)itemTurist.Tag;
            Nomer n = (Nomer)itemNomer.Tag;

            int i = 0;
            foreach (Turist t1 in Turists)
            {
                if (t1.NomerOfTurist == n)
                    {
                       i = i+1;
                    }
            }

            if (i >= n.capacity)
            {
                MessageBox.Show("Перегруженно");
                return;
            }
            

            t.NomerOfTurist = n;
            RefreshTuristInNomer(n);
        }

        private void btnCheckOut_Click(object sender, EventArgs e)
        {
            if (listTuristInNomer.SelectedItems.Count == 0)
                return;
            if (listNomer.SelectedItems.Count == 0)
                return;
            ListViewItem item1 = listTuristInNomer.SelectedItems[0];
            Turist t = (Turist)item1.Tag;
            Nomer n = t.NomerOfTurist;
            t.NomerOfTurist = null;
            RefreshTuristInNomer(n);
        }

        private void listTurist_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }

        private void listTuristInNomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableButtons();
        }


        
        
    }
}
