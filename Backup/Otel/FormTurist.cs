﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Otel
{
    public partial class FormTurist : Form
    {
        public Turist refTurist;
        public FormTurist()
        {
            InitializeComponent();
        }

        public void SetData(Turist pTurist)
        {
            txtTuristName.Text = pTurist.name;
            txtTuristLastname.Text = pTurist.lastname; 
            txtTuristPassport.Text = pTurist.passport.ToString();
            txtTuristDay.Text = pTurist.day.ToString();
            txtTuristMonth.Text = pTurist.month.ToString();
            txtTuristYear.Text = pTurist.year.ToString();
        }

        public void GetData(Turist pTurist)
        {
            pTurist.name = txtTuristName.Text;
            pTurist.lastname = txtTuristLastname.Text;
            pTurist.passport = int.Parse(txtTuristPassport.Text);
            pTurist.day = int.Parse(txtTuristDay.Text);
            pTurist.month = int.Parse(txtTuristMonth.Text);
            pTurist.year = int.Parse(txtTuristYear.Text);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtTuristName.Text = string.Empty;
            txtTuristLastname.Text = string.Empty;
            txtTuristPassport.Text = string.Empty;
            txtTuristDay.Text = string.Empty;
            txtTuristMonth.Text = string.Empty;
            txtTuristYear.Text = string.Empty;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
