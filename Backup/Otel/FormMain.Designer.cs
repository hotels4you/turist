﻿namespace Otel
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("");
            this.btnAddTurist = new System.Windows.Forms.Button();
            this.btnHotel = new System.Windows.Forms.Button();
            this.btnNomer = new System.Windows.Forms.Button();
            this.listNomer = new System.Windows.Forms.ListView();
            this.Header1 = new System.Windows.Forms.ColumnHeader();
            this.Header2 = new System.Windows.Forms.ColumnHeader();
            this.Header3 = new System.Windows.Forms.ColumnHeader();
            this.listTurist = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDelHotel = new System.Windows.Forms.Button();
            this.btnEditHotel = new System.Windows.Forms.Button();
            this.btnCheckOut = new System.Windows.Forms.Button();
            this.btnCheckIn = new System.Windows.Forms.Button();
            this.listTuristInNomer = new System.Windows.Forms.ListView();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDelTurist = new System.Windows.Forms.Button();
            this.btnEditTurist = new System.Windows.Forms.Button();
            this.btnChangeNomer = new System.Windows.Forms.Button();
            this.btnDelNomer = new System.Windows.Forms.Button();
            this.listHotel = new System.Windows.Forms.ListView();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // btnAddTurist
            // 
            this.btnAddTurist.Location = new System.Drawing.Point(804, 36);
            this.btnAddTurist.Name = "btnAddTurist";
            this.btnAddTurist.Size = new System.Drawing.Size(139, 29);
            this.btnAddTurist.TabIndex = 0;
            this.btnAddTurist.Text = "добавить туриста";
            this.btnAddTurist.UseVisualStyleBackColor = true;
            this.btnAddTurist.Click += new System.EventHandler(this.btnAddTurist_Click);
            // 
            // btnHotel
            // 
            this.btnHotel.Location = new System.Drawing.Point(346, 35);
            this.btnHotel.Name = "btnHotel";
            this.btnHotel.Size = new System.Drawing.Size(139, 22);
            this.btnHotel.TabIndex = 4;
            this.btnHotel.Text = "Добавить отель";
            this.btnHotel.UseVisualStyleBackColor = true;
            this.btnHotel.Click += new System.EventHandler(this.btnHotel_Click);
            // 
            // btnNomer
            // 
            this.btnNomer.Location = new System.Drawing.Point(346, 138);
            this.btnNomer.Name = "btnNomer";
            this.btnNomer.Size = new System.Drawing.Size(139, 28);
            this.btnNomer.TabIndex = 5;
            this.btnNomer.Text = "Добавить номер";
            this.btnNomer.UseVisualStyleBackColor = true;
            this.btnNomer.Click += new System.EventHandler(this.btnNomer_Click);
            // 
            // listNomer
            // 
            this.listNomer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Header1,
            this.Header2,
            this.Header3});
            this.listNomer.FullRowSelect = true;
            this.listNomer.GridLines = true;
            this.listNomer.HideSelection = false;
            this.listNomer.Location = new System.Drawing.Point(33, 138);
            this.listNomer.Name = "listNomer";
            this.listNomer.Size = new System.Drawing.Size(307, 96);
            this.listNomer.TabIndex = 6;
            this.listNomer.UseCompatibleStateImageBehavior = false;
            this.listNomer.View = System.Windows.Forms.View.Details;
            this.listNomer.SelectedIndexChanged += new System.EventHandler(this.listNomer_SelectedIndexChanged);
            // 
            // Header1
            // 
            this.Header1.Text = "Номер";
            this.Header1.Width = 91;
            // 
            // Header2
            // 
            this.Header2.Text = "Вместимость";
            this.Header2.Width = 124;
            // 
            // Header3
            // 
            this.Header3.Text = "Цена";
            this.Header3.Width = 85;
            // 
            // listTurist
            // 
            this.listTurist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
            this.listTurist.FullRowSelect = true;
            this.listTurist.GridLines = true;
            this.listTurist.HideSelection = false;
            this.listTurist.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem7});
            this.listTurist.Location = new System.Drawing.Point(491, 36);
            this.listTurist.Name = "listTurist";
            this.listTurist.Size = new System.Drawing.Size(307, 297);
            this.listTurist.TabIndex = 8;
            this.listTurist.UseCompatibleStateImageBehavior = false;
            this.listTurist.View = System.Windows.Forms.View.Details;
            this.listTurist.SelectedIndexChanged += new System.EventHandler(this.listTurist_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Имя";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Фамилия";
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Номер паспорта";
            this.columnHeader3.Width = 98;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Год рождения";
            this.columnHeader4.Width = 84;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(135, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Список отелей";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(135, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Список номеров";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(604, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Список туристов";
            // 
            // btnDelHotel
            // 
            this.btnDelHotel.Location = new System.Drawing.Point(345, 95);
            this.btnDelHotel.Name = "btnDelHotel";
            this.btnDelHotel.Size = new System.Drawing.Size(138, 22);
            this.btnDelHotel.TabIndex = 16;
            this.btnDelHotel.Text = "Удалить отель";
            this.btnDelHotel.UseVisualStyleBackColor = true;
            this.btnDelHotel.Click += new System.EventHandler(this.btnDelHotel_Click);
            // 
            // btnEditHotel
            // 
            this.btnEditHotel.Location = new System.Drawing.Point(346, 63);
            this.btnEditHotel.Name = "btnEditHotel";
            this.btnEditHotel.Size = new System.Drawing.Size(139, 22);
            this.btnEditHotel.TabIndex = 15;
            this.btnEditHotel.Text = "Изменить отель";
            this.btnEditHotel.UseVisualStyleBackColor = true;
            this.btnEditHotel.Click += new System.EventHandler(this.btnEditHotel_Click);
            // 
            // btnCheckOut
            // 
            this.btnCheckOut.Location = new System.Drawing.Point(346, 304);
            this.btnCheckOut.Name = "btnCheckOut";
            this.btnCheckOut.Size = new System.Drawing.Size(139, 29);
            this.btnCheckOut.TabIndex = 14;
            this.btnCheckOut.Text = "Выписать";
            this.btnCheckOut.UseVisualStyleBackColor = true;
            this.btnCheckOut.Click += new System.EventHandler(this.btnCheckOut_Click);
            // 
            // btnCheckIn
            // 
            this.btnCheckIn.Location = new System.Drawing.Point(345, 253);
            this.btnCheckIn.Name = "btnCheckIn";
            this.btnCheckIn.Size = new System.Drawing.Size(139, 29);
            this.btnCheckIn.TabIndex = 13;
            this.btnCheckIn.Text = "Поселить";
            this.btnCheckIn.UseVisualStyleBackColor = true;
            this.btnCheckIn.Click += new System.EventHandler(this.btnCheckIn_Click);
            // 
            // listTuristInNomer
            // 
            this.listTuristInNomer.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listTuristInNomer.FullRowSelect = true;
            this.listTuristInNomer.GridLines = true;
            this.listTuristInNomer.HideSelection = false;
            this.listTuristInNomer.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem8});
            this.listTuristInNomer.Location = new System.Drawing.Point(33, 253);
            this.listTuristInNomer.Name = "listTuristInNomer";
            this.listTuristInNomer.Size = new System.Drawing.Size(307, 80);
            this.listTuristInNomer.TabIndex = 12;
            this.listTuristInNomer.UseCompatibleStateImageBehavior = false;
            this.listTuristInNomer.View = System.Windows.Forms.View.Details;
            this.listTuristInNomer.SelectedIndexChanged += new System.EventHandler(this.listTuristInNomer_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Имя";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Фамилия";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Номер паспорта";
            this.columnHeader7.Width = 98;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Год рождения";
            this.columnHeader8.Width = 84;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(135, 237);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Поселенные туристы";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // btnDelTurist
            // 
            this.btnDelTurist.Location = new System.Drawing.Point(804, 106);
            this.btnDelTurist.Name = "btnDelTurist";
            this.btnDelTurist.Size = new System.Drawing.Size(139, 29);
            this.btnDelTurist.TabIndex = 13;
            this.btnDelTurist.Text = "удалить туриста";
            this.btnDelTurist.UseVisualStyleBackColor = true;
            this.btnDelTurist.Click += new System.EventHandler(this.btnDelTurist_Click);
            // 
            // btnEditTurist
            // 
            this.btnEditTurist.Location = new System.Drawing.Point(804, 71);
            this.btnEditTurist.Name = "btnEditTurist";
            this.btnEditTurist.Size = new System.Drawing.Size(139, 29);
            this.btnEditTurist.TabIndex = 12;
            this.btnEditTurist.Text = "изменит данные";
            this.btnEditTurist.UseVisualStyleBackColor = true;
            this.btnEditTurist.Click += new System.EventHandler(this.btnEditTurist_Click);
            // 
            // btnChangeNomer
            // 
            this.btnChangeNomer.Location = new System.Drawing.Point(346, 172);
            this.btnChangeNomer.Name = "btnChangeNomer";
            this.btnChangeNomer.Size = new System.Drawing.Size(139, 28);
            this.btnChangeNomer.TabIndex = 17;
            this.btnChangeNomer.Text = "Изменить номер";
            this.btnChangeNomer.UseVisualStyleBackColor = true;
            this.btnChangeNomer.Click += new System.EventHandler(this.btnChangeNomer_Click);
            // 
            // btnDelNomer
            // 
            this.btnDelNomer.Location = new System.Drawing.Point(345, 206);
            this.btnDelNomer.Name = "btnDelNomer";
            this.btnDelNomer.Size = new System.Drawing.Size(139, 28);
            this.btnDelNomer.TabIndex = 18;
            this.btnDelNomer.Text = "Удалить номер";
            this.btnDelNomer.UseVisualStyleBackColor = true;
            this.btnDelNomer.Click += new System.EventHandler(this.btnDelNomer_Click);
            // 
            // listHotel
            // 
            this.listHotel.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9});
            this.listHotel.FullRowSelect = true;
            this.listHotel.GridLines = true;
            this.listHotel.HideSelection = false;
            this.listHotel.Location = new System.Drawing.Point(34, 36);
            this.listHotel.Name = "listHotel";
            this.listHotel.Size = new System.Drawing.Size(306, 64);
            this.listHotel.TabIndex = 19;
            this.listHotel.UseCompatibleStateImageBehavior = false;
            this.listHotel.View = System.Windows.Forms.View.Details;
            this.listHotel.SelectedIndexChanged += new System.EventHandler(this.listHotel_SelectedIndexChanged);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Название";
            this.columnHeader9.Width = 200;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(949, 352);
            this.Controls.Add(this.listHotel);
            this.Controls.Add(this.btnDelNomer);
            this.Controls.Add(this.btnChangeNomer);
            this.Controls.Add(this.btnDelTurist);
            this.Controls.Add(this.btnDelHotel);
            this.Controls.Add(this.btnEditTurist);
            this.Controls.Add(this.listTurist);
            this.Controls.Add(this.btnAddTurist);
            this.Controls.Add(this.btnEditHotel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCheckOut);
            this.Controls.Add(this.listNomer);
            this.Controls.Add(this.btnCheckIn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listTuristInNomer);
            this.Controls.Add(this.btnNomer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnHotel);
            this.Name = "FormMain";
            this.Text = "Основная форма";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddTurist;
        private System.Windows.Forms.Button btnHotel;
        private System.Windows.Forms.Button btnNomer;
        private System.Windows.Forms.ListView listNomer;
        private System.Windows.Forms.ColumnHeader Header1;
        private System.Windows.Forms.ColumnHeader Header2;
        private System.Windows.Forms.ColumnHeader Header3;
        private System.Windows.Forms.ListView listTurist;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCheckIn;
        private System.Windows.Forms.ListView listTuristInNomer;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.Button btnCheckOut;
        private System.Windows.Forms.Button btnDelTurist;
        private System.Windows.Forms.Button btnEditTurist;
        private System.Windows.Forms.Button btnDelHotel;
        private System.Windows.Forms.Button btnEditHotel;
        private System.Windows.Forms.Button btnChangeNomer;
        private System.Windows.Forms.Button btnDelNomer;
        private System.Windows.Forms.ListView listHotel;
        private System.Windows.Forms.ColumnHeader columnHeader9;
    }
}